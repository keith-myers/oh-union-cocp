﻿using System;
using System.Linq;

namespace InnovativeEnterprises
{
    internal class Downloader : ThreeLetterPagingDownloaderTemplate
    {
        public Downloader(string downloadPath, string proxyUrl, string username, string password, DateTime runTimeStart, int runTimeHours) : base(downloadPath, proxyUrl, username, password, runTimeStart, runTimeHours) { }

        protected override void DoSetup()
        {
            Request = new Request("Dumb",
                                  @"GET https://eservices.co.union.oh.us/eservices HTTP/1.1
                                    Host: eservices.co.union.oh.us
                                    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0
                                    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
                                    Accept-Language: en-US,en;q=0.5
                                    Accept-Encoding: gzip, deflate, br
                                    Referer: https://www.co.union.oh.us/Public-Records-Search-Apps/
                                    Connection: keep-alive
                                    Upgrade-Insecure-Requests: 1",
                                  Proxy,
                                  CookieContainer);

            Html = Request.DownloadData();
            JSessionId = Request.ResponseUri.SnipWithoutEndChunk("jsessionid=");
            EmbeddedLink = Html.Snip("?", "\"", false);

            //-------------------------------------------------------------------------------------

            Request = new Request("Dumber",
                                  string.Format(@"POST https://eservices.co.union.oh.us/eservices/home.page;jsessionid={0}?{1} HTTP/1.1
                                                Host: eservices.co.union.oh.us
                                                User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0
                                                Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
                                                Accept-Language: en-US,en;q=0.5
                                                Accept-Encoding: gzip, deflate, br
                                                Referer: https://eservices.co.union.oh.us/eservices/home.page;jsessionid={0}
                                                Content-Type: application/x-www-form-urlencoded
                                                Content-Length: 476
                                                Connection: keep-alive
                                                Upgrade-Insecure-Requests: 1", JSessionId, EmbeddedLink),
                                  Proxy,
                                  CookieContainer);

            Html = Request.DownloadData();
            EmbeddedLink = Html.Snip("<a href=\"#\" class=\"anchorButton\" name=\"linkFrag:beginButton\" id=\"idb\" onclick=\"var wcall=wicketSubmitFormById(\'ida\', \'?", "'", false);

            //-------------------------------------------------------------------------------------

            var captchaLink = Html.Snip("<img class=\"captchaImg\" id=\"idf\" src=\"?", "\"", false).Replace("&amp;", "&");

            do
            {
                var captchaSolution = CaptchaHelper.SolveCaptchaV1(string.Format("https://eservices.co.union.oh.us/eservices/home.page.2?{0}", captchaLink), CookieContainer, Proxy, "OH Union COCP");

                Request = new Request("Dumbest",
                                      string.Format(@"POST https://eservices.co.union.oh.us/eservices/home.page.2?{0}&random=0.4881191096676767 HTTP/1.1
                                                    Host: eservices.co.union.oh.us
                                                    Connection: keep-alive
                                                    Content-Length: 70
                                                    Accept: text/xml
                                                    Origin: https://eservices.co.union.oh.us
                                                    Wicket-Ajax: true
                                                    Wicket-FocusedElementId: id3
                                                    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36
                                                    Content-Type: application/x-www-form-urlencoded
                                                    Referer: https://eservices.co.union.oh.us/eservices/home.page.2
                                                    Accept-Encoding: gzip, deflate, br
                                                    Accept-Language: en-US,en;q=0.9", EmbeddedLink),
                                      Proxy,
                                      CookieContainer);
                Request.AddHeader("Wicket-Ajax: true");
                Request.AddHeader("Wicket-FocusedElementId: id3");
                Request.AddPostData(string.Format("ida_hf_0=&captchaPanel%3AchallengePassword={0}&linkFrag%3AbeginButton=1", captchaSolution));

                Html = Request.DownloadData();

                IsGoodCaptchaAnswer = !Html.SnipExists("The answer that was given did not match the text in the picture. Please try again.");
                if (!IsGoodCaptchaAnswer)
                {
                    HandleBadCaptchaAnswer();
                }

            } while (!IsGoodCaptchaAnswer);

            EmbeddedLink = Html.Snip("search.page?x=", "]", false);

            //-------------------------------------------------------------------------------------

            Request = new Request("Finally Redirect to Search Page",
                                  string.Format(@"GET https://eservices.co.union.oh.us/eservices/search.page?{0} HTTP/1.1
                                                Host: eservices.co.union.oh.us
                                                Connection: keep-alive
                                                Upgrade-Insecure-Requests: 1
                                                User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36
                                                Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
                                                Referer: https://eservices.co.union.oh.us/eservices/home.page.2
                                                Accept-Encoding: gzip, deflate, br
                                                Accept-Language: en-US,en;q=0.9", EmbeddedLink),
                                  Proxy,
                                  CookieContainer);

            Html = Request.DownloadData();
            EmbeddedLink = Html.Snip("method=\"post\" action=\"?", "\"", false);
        }

        protected override void DownloadFirstPageAndSetupPager()
        {
            Request = new Request(string.Format("Search Page 1 for {0}", LetterSearchLastName),
                                  string.Format(@"POST https://eservices.co.union.oh.us/eservices/search.page.7?{0} HTTP/1.1
                                    Host: eservices.co.union.oh.us
                                    Connection: keep-alive
                                    Content-Length: 351
                                    Cache-Control: max-age=0
                                    Origin: https://eservices.co.union.oh.us
                                    Upgrade-Insecure-Requests: 1
                                    Content-Type: application/x-www-form-urlencoded
                                    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36
                                    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
                                    Accept-Encoding: gzip, deflate, br
                                    Accept-Language: en-US,en;q=0.9", EmbeddedLink),
                                  Proxy,
                                  CookieContainer);

            Request.AddPostData("idb4_hf_0=");
            Request.AddPostData("&lastName=", LetterSearchLastName, false);
            Request.AddPostData("&firstName=");
            Request.AddPostData("&middleName=");
            Request.AddPostData("&sffxCd=");
            Request.AddPostData("&companyName=");
            Request.AddPostData("&caseCd=CR++++++++++++++++++++++++++++");
            Request.AddPostData("&statCd=+");
            Request.AddPostData("&ptyCd=DFNDT+++++++++++++++++++++++++");
            Request.AddPostData("&dobDateRange%3AdateInputBegin=");
            Request.AddPostData("&dobDateRange%3AdateInputEnd=");
            Request.AddPostData("&dodDateRange%3AdateInputBegin=");
            Request.AddPostData("&dodDateRange%3AdateInputEnd=");
            Request.AddPostData("&fileDateRange%3AdateInputBegin=");
            Request.AddPostData("&fileDateRange%3AdateInputEnd=");
            Request.AddPostData("&submitLink=Search");

            Html = Request.DownloadData();

            // If no records, nothing to do.
            if (Html.SnipExists("No Matches Found"))
            {
                return;
            }

            // Setup pager
            SetupPager("<span>Showing", "</span>", 25);

            // Get the dreaded embedded link...
            EmbeddedLink = Html.SnipInReverse("\" title=\"Go to next page\"", "?");

            // Process this page of data.
            ProcessRecords();
        }

        protected override void DownloadNextPage()
        {
            Request = new Request(string.Format("Search Page {0} of {1} for {2}", NextPage, NumPages, LetterSearchLastName),
                                  string.Format(@"GET https://eservices.co.union.oh.us/eservices/search.page.10?{0} HTTP/1.1
                                    Host: eservices.co.union.oh.us
                                    Connection: keep-alive
                                    Upgrade-Insecure-Requests: 1
                                    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36
                                    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
                                    Accept-Encoding: gzip, deflate, br
                                    Accept-Language: en-US,en;q=0.9", EmbeddedLink),
                                  Proxy,
                                  CookieContainer);

            Html = Request.DownloadData();

            // Get the dreaded embedded link...
            EmbeddedLink = Html.SnipInReverse("\" title=\"Go to next page\"", "?");

            // Process this page of data.
            ProcessRecords();
        }

        protected override void ProcessRecords()
        {
            var table = Html.Snip("<table id=\"grid\" summary=\"Search Results Grid\" class=\"tableResults\">", "</table>", true);
            var header = new CQuery(table).GetByElementName("THEAD").GetByElementName("TR").Selection;
            var rows = new CQuery(table).GetByElementName("TBODY").GetByElementName("TR").Selections;

            foreach (var row in rows)
            {
                var cells = new CQuery(row).GetAllTableCellText().Single();

                DetailData["name"] = new CQuery(cells[1]).GetByElementName("A").GetByElementName("SPAN").Text();
                DetailData["caseNumber"] = new CQuery(cells[4]).GetByElementName("A").GetByElementName("SPAN").Text();
                DetailData["detailLink"] = cells[4].Snip("<a href=\"?", "\"", false);
                DetailData["fileDate"] = new CQuery(cells[5]).GetByElementName("A").GetByElementName("SPAN").Text().RemoveNonAlphaNumericCharacters();
                DetailData["header"] = CQuery.CreateDetailHeaderTable(header, row);

                DownloadMainDetail();
            }
        }

        protected override void DownloadMainDetail()
        {
            Request = new Request(string.Format("Downloading detail for {0}", DetailData["name"]),
                                  string.Format(@"GET https://eservices.co.union.oh.us/eservices/search.page.16?{0} HTTP/1.1
                                                Host: eservices.co.union.oh.us
                                                Connection: keep-alive
                                                Upgrade-Insecure-Requests: 1
                                                User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36
                                                Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
                                                Accept-Encoding: gzip, deflate, br
                                                Accept-Language: en-US,en;q=0.9", DetailData["detailLink"]),
                                  Proxy,
                                  CookieContainer);

            var detailHtml = Request.DownloadData();

            // Download additional charges if they exist.
            var indexOfLoadAll = detailHtml.IndexOf("Load All");
            if (indexOfLoadAll != -1)
            {
                var startIndex = detailHtml.LastIndexOf("?x=", indexOfLoadAll);
                var endIndex = detailHtml.IndexOf("'", startIndex);
                var additionalChargesLink = detailHtml.Substring(startIndex, endIndex - startIndex);

                Request = new Request("Downloading additional charges embedded link",
                                      string.Format(@"GET https://eservices.co.union.oh.us/eservices/search.page.10{0}&random={1} HTTP/1.1
                                                    Host: eservices.co.union.oh.us
                                                    Connection: keep-alive
                                                    Accept: text/xml
                                                    Wicket-Ajax: true
                                                    Wicket-FocusedElementId: id2f8
                                                    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36
                                                    Accept-Encoding: gzip, deflate, br
                                                    Accept-Language: en-US,en;q=0.9", additionalChargesLink, GetRandomDoublePaddedTo18Digits()),
                                      Proxy,
                                      CookieContainer);
                Request.AddHeader("Wicket-Ajax: true");
                Request.AddHeader("Wicket-FocusedElementId: id2f8");

                var additionalChargesHtml = Request.DownloadData();

                //---------------------------------------------------------------------------------

                additionalChargesLink = additionalChargesHtml.Snip("?x=", "'", false);

                Request = new Request("Downloading additional charges embedded link",
                                      string.Format(@"GET https://eservices.co.union.oh.us/eservices/search.page.10?x={0}&random={1} HTTP/1.1
                                                    Host: eservices.co.union.oh.us
                                                    Connection: keep-alive
                                                    Accept: text/xml
                                                    Wicket-Ajax: true
                                                    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36
                                                    Accept-Encoding: gzip, deflate, br
                                                    Accept-Language: en-US,en;q=0.9", additionalChargesLink, GetRandomDoublePaddedTo18Digits()),
                                      Proxy,
                                      CookieContainer);
                Request.AddHeader("Wicket-Ajax: true");

                additionalChargesHtml = Request.DownloadData();
                detailHtml = detailHtml.StoreData("additionalCharges", additionalChargesHtml);
            }

            detailHtml = detailHtml.StoreData("HeaderData", DetailData["header"]);
            SaveHtml(detailHtml, DetailData["name"], DetailData["caseNumber"], DetailData["fileDate"]);
        }
    }
}
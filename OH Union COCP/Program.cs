﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace InnovativeEnterprises
{
    internal class Program
    {
        private static void Main()
        {
            var executingAssemblyName = Assembly.GetExecutingAssembly().GetName().Name;
            var executingDirectoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var downloadPath = executingDirectoryName.Substring(0, executingDirectoryName.IndexOf(executingAssemblyName) + executingAssemblyName.Length) + @"\Data\Temp";
            const int runTimeHours = 10;

            var allLetters = new Dictionary<string, int>();
            for (var index = 65; index <= 90; index++)
            {
                allLetters[((char) index).ToString()] = index;
            }

            //var letter1 = allLetters["M"];
            //var letter2 = allLetters["Y"];
            //var letter3 = allLetters["E"];

            //var letter1 = allLetters["Z"];
            //var letter2 = allLetters["Z"];
            //var letter3 = allLetters["Z"];

            var letter1 = allLetters["P"];
            var letter2 = allLetters["H"];
            var letter3 = allLetters["E"];

            for (var i = letter1; i <= 90; i++)
            {
                for (var j = letter2; j <= 90; j++)
                {
                    for (var k = letter3; k <= 90; k++)
                    {
                        var downloader = new Downloader(downloadPath, "http://rotator1.x5.net:5", "knowthefacts-USA", "com", DateTime.Now, runTimeHours);
                        downloader.Initialize(((char) i).ToString(), ((char) j).ToString(), ((char) k).ToString());
                        var result = downloader.Run();

                        switch (result)
                        {
                            case 0:
                                // Close batch.
                                // Exit gracefully in ScriptMain.
                                break;
                            case -1:
                                // Ran out of time.
                                // Don't close batch.
                                // Throw 9:00 a.m. exception in ScriptMain.
                                break;
                            case -2:
                                // Throw 9:00 a.m. exception in ScriptMain.
                                break;
                        }
                    }
                    letter3 = 65;
                }
                letter2 = 65;
            }
        }
    }
}
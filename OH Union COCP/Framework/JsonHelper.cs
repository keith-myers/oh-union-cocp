﻿using System.Web.Script.Serialization;

namespace InnovativeEnterprises
{
    internal static class JsonHelper
    {
        public static object GetJsonObject(string jsonString)
        {
            var jsSerializer = new JavaScriptSerializer();
            return jsSerializer.DeserializeObject(jsonString);
        }
    }
}
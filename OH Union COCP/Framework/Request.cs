﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;

namespace InnovativeEnterprises
{
    public class Request
    {
        private const int SLEEP_TIME = 2500;
        private readonly string _name;
        public HttpWebRequest HttpWebRequest { get; private set; }
        public string RequestUri { get; set; }
        public string ResponseUri { get; set; }

        public Request(string name, string raw, IWebProxy webProxy, CookieContainer cookieContainer)
        {
            _name = name;

            raw = raw.Replace("HTTP/1.1", "");

            var chunks = raw.Split(new[] {"\r\n"}, StringSplitOptions.None);
            var firstChunk = chunks[0];

            if (firstChunk.Contains("GET"))
            {
                HttpWebRequest = (HttpWebRequest) WebRequest.Create(firstChunk.Replace("GET", ""));
                HttpWebRequest.Method = "GET";
            }
            else if (firstChunk.Contains("POST"))
            {
                HttpWebRequest = (HttpWebRequest) WebRequest.Create(firstChunk.Replace("POST", ""));
                HttpWebRequest.Method = "POST";
            }
            else
            {
                throw new Exception("First chunk does not contain GET or POST!");
            }

            RequestUri = HttpWebRequest.RequestUri.ToString();
            HttpWebRequest.CookieContainer = cookieContainer;
            HttpWebRequest.Timeout = (int) TimeSpan.FromMinutes(5).TotalMilliseconds;
            HttpWebRequest.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;

            for (var i = 1; i < chunks.Length; i++)
            {
                var chunk = chunks[i];

                if (chunk.IndexOf("User-Agent", StringComparison.OrdinalIgnoreCase) != -1)
                {
                    HttpWebRequest.UserAgent = chunk.Split(':')[1];
                }

                if (chunk.IndexOf("Referer", StringComparison.OrdinalIgnoreCase) != -1)
                {
                    HttpWebRequest.Referer = chunk.Split(new[] {':'}, 2)[1];
                }

                if (chunk.IndexOf("Content-Type", StringComparison.OrdinalIgnoreCase) != -1)
                {
                    HttpWebRequest.ContentType = chunk.Split(':')[1];
                }

                // Headers

                if (chunk.IndexOf("Cache-Control", StringComparison.OrdinalIgnoreCase) != -1)
                {
                    AddHeader(chunk);
                }

                if (chunk.IndexOf("Origin", StringComparison.OrdinalIgnoreCase) != -1)
                {
                    var origin = chunk.Split(new[] {':'}, 2)[1];
                    AddHeader("Origin", origin);
                }

                if (chunk.IndexOf("Upgrade-Insecure-Requests", StringComparison.OrdinalIgnoreCase) != -1)
                {
                    AddHeader(chunk);
                }

                if (chunk.IndexOf("Accept-Language", StringComparison.OrdinalIgnoreCase) != -1)
                {
                    AddHeader(chunk);
                }
                else if (chunk.IndexOf("Accept-Encoding", StringComparison.OrdinalIgnoreCase) != -1) /* Do nothing for now */
                {
                    ;
                }
                else if (chunk.IndexOf("Accept", StringComparison.OrdinalIgnoreCase) != -1)
                {
                    HttpWebRequest.Accept = chunk.Split(':')[1];
                }
            }

            if (webProxy != null)
            {
                HttpWebRequest.Proxy = webProxy;
            }
        }

        public int ReadWriteTimeout
        {
            get { return HttpWebRequest.ReadWriteTimeout; }
            set { HttpWebRequest.ReadWriteTimeout = value; }
        }

        public int ConnectionLeaseTimeout
        {
            get { return HttpWebRequest.ServicePoint.ConnectionLeaseTimeout; }
            set { HttpWebRequest.ServicePoint.ConnectionLeaseTimeout = value; }
        }

        public int MaxIdleTime
        {
            get { return HttpWebRequest.ServicePoint.MaxIdleTime; }
            set { HttpWebRequest.ServicePoint.MaxIdleTime = value; }
        }

        private bool _needs417WorkAround;
        public bool Needs417WorkAround
        {
            set { _needs417WorkAround = value; }
        }

        private bool _needsExpect100ContinueWorkAround;
        public bool NeedsExpect100ContinueWorkAround
        {
            set { _needsExpect100ContinueWorkAround = value; }
        }

        private bool _requiresSSL;
        public bool RequiresSSL
        {
            set { _requiresSSL = true; }
        }

        public IWebProxy Proxy
        {
            get { return HttpWebRequest.Proxy; }
            set { HttpWebRequest.Proxy = value; }
        }

        public string Accept
        {
            get { return HttpWebRequest.Accept; }
            set { HttpWebRequest.Accept = value; }
        }

        public string UserAgent
        {
            get { return HttpWebRequest.UserAgent; }
            set { HttpWebRequest.UserAgent = value; }
        }

        public int Timeout
        {
            get { return HttpWebRequest.Timeout; }
            set { HttpWebRequest.Timeout = value; }
        }

        public CookieContainer CookieContainer
        {
            get { return HttpWebRequest.CookieContainer; }
            set { HttpWebRequest.CookieContainer = value; }
        }

        public string ContentType
        {
            get { return HttpWebRequest.ContentType; }
            set { HttpWebRequest.ContentType = value; }
        }

        public string Referer
        {
            get { return HttpWebRequest.Referer; }
            set { HttpWebRequest.Referer = value; }
        }

        private string _postData = "";

        public Request AddPostData(string postData)
        {
            _postData += postData;

            return this;
        }

        public Request AddPostData(string name, string value, bool valueNeedsUrlEncoding)
        {
            _postData += string.Format("{0}{1}", name, valueNeedsUrlEncoding ? HttpUtility.UrlEncode(value) : value);

            return this;
        }

        public Request AddHeader(string header)
        {
            var chunks = header.Split(new[] {":"}, 2, StringSplitOptions.None);
            var name = chunks[0].Trim();
            var value = chunks[1].Trim();

            return AddHeader(name, value);
        }

        public Request AddHeader(string name, string value)
        {
            HttpWebRequest.Headers.Add(name, value);

            return this;
        }

        public string DownloadData()
        {
            DownloadHelper.WriteLine(string.Format("Downloading data for: {0}...", _name));

            if (!DownloadHelper.IsRunningLocally)
            {
                Thread.Sleep(SLEEP_TIME);
            }

            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            if (_requiresSSL)
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
            }
            else
            {
                ServicePointManager.SecurityProtocol = (SecurityProtocolType) 3072;
            }

            if (_needs417WorkAround)
            {
                ServicePointManager.Expect100Continue = false;
                HttpWebRequest.ProtocolVersion = HttpVersion.Version10;
            }

            if (_needsExpect100ContinueWorkAround)
            {
                ServicePointManager.Expect100Continue = false;
            }

            if (HttpWebRequest.Method == RequestMethod.POST.Name)
            {
                var content = Encoding.ASCII.GetBytes(_postData);
                var contentLength = content.Length;
                HttpWebRequest.ContentLength = contentLength;

                using (var stream = HttpWebRequest.GetRequestStream())
                {
                    stream.Write(content, 0, contentLength);
                }
            }

            using (var response = (HttpWebResponse) HttpWebRequest.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    if (stream == null)
                    {
                        throw new InvalidOperationException();
                    }

                    using (var reader = new StreamReader(stream))
                    {
                        var html = reader.ReadToEnd().Trim();
                        if (!string.IsNullOrEmpty(html))
                        {
                            ResponseUri = response.ResponseUri.OriginalString;
                            return html;
                        }

                        throw new Exception(string.Format("Failed to retrieve {0}", _name));
                    }
                }
            }
        }

        public Image DownloadPhoto()
        {
            DownloadHelper.WriteLine(string.Format("Downloading photo of: {0}...", _name));

            if (!DownloadHelper.IsRunningLocally)
            {
                Thread.Sleep(SLEEP_TIME);
            }

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;

            if (_requiresSSL)
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
            }
            else
            {
                ServicePointManager.SecurityProtocol = (SecurityProtocolType) 3072;
            }

            using (var response = (HttpWebResponse) HttpWebRequest.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        stream.CopyTo(memoryStream);

                        if (memoryStream.Length == 4248)
                        {
                            return null;
                        }

                        using (var image = Image.FromStream(memoryStream))
                        {
                            return new Bitmap(image);
                        }
                    }
                }
            }
        }
    }
}
using System;

namespace InnovativeEnterprises
{
    public class Person
    {
        private string _iD;
        private string _category;
        private string _source;
        private string _sourceState;
        private string _sourceName;
        private string _lastName;
        private string _firstName;
        private string _middleName;
        private string _generation;
        private string _dOB;
        private string _sSN;
        private string _lastEx;
        private string _dOBNew;
        private string _firstStrip;
        private string _lastStrip;
        private string _birthState;
        private string _aKA1;
        private string _aKA2;
        private string _dobAKA;
        private string _address1;
        private string _address2;
        private string _city;
        private string _state;
        private string _zip;
        private string _age;
        private string _hair;
        private string _eye;
        private string _height;
        private string _weight;
        private string _race;
        private string _scarsMarks;
        private string _sex;
        private string _skinTone;
        private string _militaryService;
        private string _expungement_Flag;
        private string _aKAFlag;

        public Person(string source, string sourceName, string sourceState, string category, string sex, string age)
        {
            Source = source;
            SourceName = sourceName;
            SourceState = sourceState;
            Category = category;
            Sex = sex;
            Age = age;
        }

        public string ID
        {
            get { return _iD; }
            set { _iD = value.ScrubForDatabase(); }
        }
        public string Category
        {
            get { return _category; }
            set { _category = value.ScrubForDatabase(); }
        }
        public string Source
        {
            get { return _source; }
            set { _source = value.ScrubForDatabase(); }
        }
        public string SourceState
        {
            get { return _sourceState; }
            set { _sourceState = value.ScrubForDatabase(); }
        }
        public string SourceName
        {
            get { return _sourceName; }
            set { _sourceName = value.ScrubForDatabase(); }
        }
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value.ScrubForDatabase(); }
        }
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value.ScrubForDatabase(); }
        }
        public string MiddleName
        {
            get { return _middleName; }
            set { _middleName = value.ScrubForDatabase(); }
        }
        public string Generation
        {
            get { return _generation; }
            set { _generation = value.ScrubForDatabase(); }
        }
        public string DOB
        {
            get { return _dOB; }
            set { _dOB = value.ScrubForDatabase().FormatDateAsyyyyMMdd(); }
        }
        public string SSN
        {
            get { return _sSN; }
            set { _sSN = value.ScrubForDatabase(); }
        }
        public string LastEx
        {
            get { return _lastEx; }
            set { _lastEx = value.ScrubForDatabase(); }
        }
        public string DOBNew
        {
            get { return _dOBNew; }
            set { _dOBNew = value.ScrubForDatabase().FormatDateAsyyyyMMdd(); }
        }
        public string FirstStrip
        {
            get { return _firstStrip; }
            set { _firstStrip = value.ScrubForDatabase(); }
        }
        public string LastStrip
        {
            get { return _lastStrip; }
            set { _lastStrip = value.ScrubForDatabase(); }
        }
        public int? DOBMonth { get; set; }
        public int? DOBDay { get; set; }
        public int? DOBYear { get; set; }
        public string BirthState
        {
            get { return _birthState; }
            set { _birthState = value.ScrubForDatabase(); }
        }
        public string AKA1
        {
            get { return _aKA1; }
            set { _aKA1 = value.ScrubForDatabase(); }
        }
        public string AKA2
        {
            get { return _aKA2; }
            set { _aKA2 = value.ScrubForDatabase(); }
        }
        public string DobAKA
        {
            get { return _dobAKA; }
            set { _dobAKA = value.ScrubForDatabase(); }
        }
        public string Address1
        {
            get { return _address1; }
            set { _address1 = value.ScrubForDatabase(); }
        }
        public string Address2
        {
            get { return _address2; }
            set { _address2 = value.ScrubForDatabase(); }
        }
        public string City
        {
            get { return _city; }
            set { _city = value.ScrubForDatabase(); }
        }
        public string State
        {
            get { return _state; }
            set { _state = value.ScrubForDatabase(); }
        }
        public string Zip
        {
            get { return _zip; }
            set { _zip = value.ScrubForDatabase(); }
        }
        public string Age
        {
            get { return _age; }
            set { _age = value.ScrubForDatabase(); }
        }
        public string Hair
        {
            get { return _hair; }
            set { _hair = value.ScrubForDatabase(); }
        }
        public string Eye
        {
            get { return _eye; }
            set { _eye = value.ScrubForDatabase(); }
        }
        public string Height
        {
            get { return _height; }
            set { _height = value.ScrubForDatabase(); }
        }
        public string Weight
        {
            get { return _weight; }
            set { _weight = value.ScrubForDatabase(); }
        }
        public string Race
        {
            get { return _race; }
            set { _race = value.ScrubForDatabase(); }
        }
        public string ScarsMarks
        {
            get { return _scarsMarks; }
            set { _scarsMarks = value.ScrubForDatabase(); }
        }
        public string Sex
        {
            get { return _sex; }
            set { _sex = value.ScrubForDatabase(); }
        }
        public string SkinTone
        {
            get { return _skinTone; }
            set { _skinTone = value.ScrubForDatabase(); }
        }
        public string MilitaryService
        {
            get { return _militaryService; }
            set { _militaryService = value.ScrubForDatabase(); }
        }
        public DateTime? DateAdded { get; set; }
        public int? SourceInfoID { get; set; }
        public int? BatchID { get; set; }
        public string Expungement_Flag
        {
            get { return _expungement_Flag; }
            set { _expungement_Flag = value.ScrubForDatabase(); }
        }
        public DateTime? Updated { get; set; }
        public string AKAFlag
        {
            get { return _aKAFlag; }
            set { _aKAFlag = value.ScrubForDatabase(); }
        }
    }
}
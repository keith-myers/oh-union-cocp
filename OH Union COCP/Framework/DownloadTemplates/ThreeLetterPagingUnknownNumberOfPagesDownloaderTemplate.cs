﻿using System;
using System.Collections.Generic;

namespace InnovativeEnterprises
{
    internal abstract class ThreeLetterPagingUnknownNumberOfPagesDownloaderTemplate : DownloadHelper
    {
        protected ThreeLetterPagingUnknownNumberOfPagesDownloaderTemplate(string downloadPath, string proxyUrl, string username, string password, DateTime runTimeStart, int runTimeHours)
            : base(downloadPath, proxyUrl, username, password, runTimeStart, runTimeHours) { }

        public void Initialize(string letter1, string letter2, string letter3)
        {
            LetterSearchLastName = letter1 + letter2 + letter3;
        }

        internal int Run()
        {
            try
            {
                if (HasReachedRunTimeLimit())
                {
                    return -1;
                }

                if (IsTimeForAmShutoff())
                {
                    return -2;
                }

                DoSetup();

                var hasAnotherPage = DownloadFirstPageAndSetupPager();
                if (!hasAnotherPage)
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                HandleError(ex);
                Run();
                return 0;
            }

            return RunPager();
        }

        private int RunPager()
        {
            while (true)
            {
                try
                {
                    if (HasReachedRunTimeLimit())
                    {
                        return -1;
                    }
                    if (IsTimeForAmShutoff())
                    {
                        return -2;
                    }

                    var hasAnotherPage = DownloadNextPage();
                    if (!hasAnotherPage)
                    {
                        break;
                    }

                    NextPage++;
                }
                catch (Exception ex)
                {
                    HandleError(ex);
                    RunPager();
                    break;
                }
            }

            return 0;
        }

        /*
         * The only methods stubs are those required for flow control.
         * Implementation of detail downloads and helper methods are
         * specific to the scrape and the developers responsibility.
         */

        protected abstract void DoSetup();
        protected abstract bool DownloadFirstPageAndSetupPager();
        protected abstract bool DownloadNextPage();
        protected abstract bool ProcessRecords();
        protected abstract void DownloadMainDetail(Dictionary<string, string> detailData);
    }
}
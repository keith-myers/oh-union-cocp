﻿using System;

namespace InnovativeEnterprises
{
    internal abstract class ThreeLetterPagingDownloaderTemplate : DownloadHelper
    {
        protected ThreeLetterPagingDownloaderTemplate(string downloadPath, string proxyUrl, string username, string password, DateTime runTimeStart, int runTimeHours)
            : base(downloadPath, proxyUrl, username, password, runTimeStart, runTimeHours) { }

        public void Initialize(string letter1, string letter2, string letter3)
        {
            LetterSearchLastName = letter1 + letter2 + letter3;
        }

        internal int Run()
        {
            try
            {
                if (HasReachedRunTimeLimit())
                {
                    return -1;
                }

                if (IsTimeForAmShutoff())
                {
                    return -2;
                }

                DoSetup();
                DownloadFirstPageAndSetupPager();
            }
            catch (Exception ex)
            {
                HandleError(ex);
                Run();
                return 0;
            }

            return NumPages > 1 ? RunPager() : 0;
        }

        private int RunPager()
        {
            while (NextPage <= NumPages)
            {
                try
                {
                    if (HasReachedRunTimeLimit())
                    {
                        return -1;
                    }

                    if (IsTimeForAmShutoff())
                    {
                        return -2;
                    }

                    DownloadNextPage();
                    NextPage++;
                }
                catch (Exception ex)
                {
                    HandleError(ex);
                    RunPager();
                    break;
                }
            }

            return 0;
        }

        /*
         * The only methods stubs are those required for flow control.
         * Implementation of detail downloads and helper methods are
         * specific to the scrape and the developers responsibility.
         */

        protected abstract void DoSetup();
        protected abstract void DownloadFirstPageAndSetupPager();
        protected abstract void DownloadNextPage();
        protected abstract void ProcessRecords();
        protected abstract void DownloadMainDetail();
        protected virtual void DownloadPhoto(){}
    }
}
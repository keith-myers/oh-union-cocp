﻿using System;
using System.Collections.Generic;

namespace InnovativeEnterprises
{
    internal abstract class FourLetterNoPagingDownloaderTemplate : DownloadHelper
    {
        protected FourLetterNoPagingDownloaderTemplate(string downloadPath, string proxyUrl, string username, string password, DateTime runTimeStart, int runTimeHours)
            : base(downloadPath, proxyUrl, username, password, runTimeStart, runTimeHours) { }

        public void Initialize(string letter1, string letter2, string letter3, string letter4)
        {
            LetterSearchLastName = letter1 + letter2 + letter3;
            LetterSearchFirstName = letter4;
        }

        internal int Run()
        {
            try
            {
                if (HasReachedRunTimeLimit())
                {
                    return -1;
                }

                if (IsTimeForAmShutoff())
                {
                    return -2;
                }

                DoSetup();
                Download();
                ProcessRecords();
            }
            catch (Exception ex)
            {
                HandleError(ex);
                Run();
                return 0;
            }

            return 0;
        }

        /*
         * The only methods stubs are those required for flow control.
         * Implementation of detail downloads and helper methods are
         * specific to the scrape and the developers responsibility.
         */

        protected abstract void DoSetup();
        protected abstract void Download();
        protected abstract void ProcessRecords();
        protected abstract void DownloadMainDetail(Dictionary<string, string> detailData);
    }
}
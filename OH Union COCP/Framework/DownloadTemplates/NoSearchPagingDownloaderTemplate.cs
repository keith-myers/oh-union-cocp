﻿using System;

namespace InnovativeEnterprises
{
    internal abstract class NoSearchPagingDownloaderTemplate : DownloadHelper
    {
        protected NoSearchPagingDownloaderTemplate(string downloadPath, string proxyUrl, string username, string password, DateTime runTimeStart, int runTimeHours)
            : base(downloadPath, proxyUrl, username, password, runTimeStart, runTimeHours) { }

        internal int Run()
        {
            try
            {
                if (HasReachedRunTimeLimit())
                {
                    return -1;
                }

                if (IsTimeForAmShutoff())
                {
                    return -2;
                }

                DoSetup();
                HitDownloadPageAndSetupPager();
            }
            catch (Exception ex)
            {
                HandleError(ex);
                Run();
                return 0;
            }

            if (NumPages > 1)
            {
                return RunPager();
            }

            return 0;
        }

        private int RunPager()
        {
            while (NextPage <= NumPages)
            {
                try
                {
                    if (HasReachedRunTimeLimit())
                    {
                        return -1;
                    }
                    if (IsTimeForAmShutoff())
                    {
                        return -2;
                    }

                    DownloadNextPage();
                    NextPage++;
                }
                catch (Exception ex)
                {
                    HandleError(ex);
                    RunPager();
                    break;
                }
            }

            return 0;
        }

        /*
         * The only methods stubs are those required for flow control.
         * Implementation of detail downloads and helper methods are
         * specific to the scrape and the developers responsibility.
         */

        protected abstract void DoSetup();
        protected abstract void HitDownloadPageAndSetupPager();
        protected abstract void DownloadNextPage();
    }
}
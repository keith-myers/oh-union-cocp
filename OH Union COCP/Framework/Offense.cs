using System;

namespace InnovativeEnterprises
{
    public class Offense
    {
        public Offense(Person person, string countyOrJurisdiction)
        {
            IDCaseNumber = person.ID.ScrubForDatabase();
            Source = person.Source.ScrubForDatabase();
            SourceName = person.SourceName.ScrubForDatabase();
            SourceState = person.SourceState.ScrubForDatabase();
            Category = person.Category.ScrubForDatabase();
            CountyOrJurisdiction = countyOrJurisdiction.ScrubForDatabase();
            Counts = 1;
        }

        private string _arrestingAgency;
        private string _caseType;
        private string _offenseDesc1;
        private string _idCaseNumber;
        private string _sourceCasenumber;
        private string _category;
        private string _source;
        private string _sourceState;
        private string _sourceName;
        private string _chargesFiledDate;
        private string _offenseDate;
        private string _offenseCode;
        private string _ncicCode;
        private string _offenseDesc2;
        private string _plea;
        private string _convictionDate;
        private string _convictionPlace;
        private string _courtCosts;
        private string _dispositionDate;
        private string _disposition;
        private string _court;
        private string _photoName;
        private string _probationYyymmddd;
        private string _sentenceYyymmddd;
        private string _caseTypeClassification;
        private string _expungement_Flag;
        private string _comment2;
        private string _comment1;
        private string _suspendedSentence;
        private string _restitution;
        private string _status;
        private string _amendedDispositionDate;
        private string _amendedDisposition;
        private string _amendedCaseType;
        private string _amendedCharge;
        private string _dLNumber;
        private string _bookingNumber;
        private string _fBINumber;
        private string _releaseDate;
        private string _commitmentDate;
        private string _commitmentState;
        private string _commitmentCounty;
        private string _commitmentLocation;
        private string _supervisionDate;
        private string _supervisionState;
        private string _supervisionCounty;
        private string _warrantState;
        private string _warrantCounty;
        private string _warrantDate;
        private string _arrestDate;
        private string _countyOrJurisdiction;
        private string _fines;

        public string IDCaseNumber
        {
            get { return _idCaseNumber; }
            set { _idCaseNumber = value.ScrubForDatabase(); }
        }

        public string Source_Casenumber
        {
            get { return _sourceCasenumber; }
            set { _sourceCasenumber = value.ScrubForDatabase(); }
        }

        public string Category
        {
            get { return _category; }
            set { _category = value.ScrubForDatabase(); }
        }

        public string Source
        {
            get { return _source; }
            set { _source = value.ScrubForDatabase(); }
        }

        public string SourceState
        {
            get { return _sourceState; }
            set { _sourceState = value.ScrubForDatabase(); }
        }

        public string SourceName
        {
            get { return _sourceName; }
            set { _sourceName = value.ScrubForDatabase(); }
        }

        public string ChargesFiledDate
        {
            get { return _chargesFiledDate; }
            set { _chargesFiledDate = value.ScrubForDatabase().FormatDateAsyyyyMMdd(); }
        }

        public string OffenseDate
        {
            get { return _offenseDate; }
            set { _offenseDate = value.ScrubForDatabase().FormatDateAsyyyyMMdd(); }
        }

        public string OffenseCode
        {
            get { return _offenseCode; }
            set { _offenseCode = value.ScrubForDatabase(); }
        }

        public string NCICCode
        {
            get { return _ncicCode; }
            set { _ncicCode = value.ScrubForDatabase(); }
        }

        public string OffenseDesc1
        {
            get { return _offenseDesc1; }
            set { _offenseDesc1 = value.ScrubForDatabase(); }
        }

        public string OffenseDesc2
        {
            get { return _offenseDesc2; }
            set { _offenseDesc2 = value.ScrubForDatabase(); }
        }

        public int? Counts { get; set; }

        public string Plea
        {
            get { return _plea; }
            set { _plea = value.ScrubForDatabase(); }
        }

        public string ConvictionDate
        {
            get { return _convictionDate; }
            set { _convictionDate = value.ScrubForDatabase().FormatDateAsyyyyMMdd(); }
        }

        public string ConvictionPlace
        {
            get { return _convictionPlace; }
            set { _convictionPlace = value.ScrubForDatabase(); }
        }

        public string SentenceYYYMMDDD
        {
            get { return _sentenceYyymmddd; }
            set { _sentenceYyymmddd = value.ScrubForDatabase(); }
        }

        public string ProbationYYYMMDDD
        {
            get { return _probationYyymmddd; }
            set { _probationYyymmddd = value.ScrubForDatabase(); }
        }

        public string PhotoName
        {
            get { return _photoName; }
            set { _photoName = value.ScrubForDatabase(); }
        }

        public string Court
        {
            get { return _court; }
            set { _court = value.ScrubForDatabase(); }
        }

        public string Disposition
        {
            get { return _disposition; }
            set { _disposition = value.ScrubForDatabase(); }
        }

        public string DispositionDate
        {
            get { return _dispositionDate; }
            set { _dispositionDate = value.ScrubForDatabase().FormatDateAsyyyyMMdd(); }
        }

        public string CourtCosts
        {
            get { return _courtCosts; }
            set { _courtCosts = value.ScrubForDatabase(); }
        }

        public string ArrestingAgency
        {
            get { return _arrestingAgency; }
            set { _arrestingAgency = value.Replace("'", "''"); }
        }

        public string CaseType
        {
            get { return _caseType; }
            set { _caseType = value.ScrubForDatabase(); }
        }

        public string Fines
        {
            get { return _fines; }
            set { _fines = value.ScrubForDatabase(); }
        }

        public string CountyOrJurisdiction
        {
            get { return _countyOrJurisdiction; }
            set { _countyOrJurisdiction = value.ScrubForDatabase(); }
        }

        public string ArrestDate
        {
            get { return _arrestDate; }
            set { _arrestDate = value.ScrubForDatabase().FormatDateAsyyyyMMdd(); }
        }

        public string WarrantDate
        {
            get { return _warrantDate; }
            set { _warrantDate = value.ScrubForDatabase().FormatDateAsyyyyMMdd(); }
        }

        public string WarrantCounty
        {
            get { return _warrantCounty; }
            set { _warrantCounty = value.ScrubForDatabase(); }
        }

        public string WarrantState
        {
            get { return _warrantState; }
            set { _warrantState = value.ScrubForDatabase(); }
        }

        public string SupervisionCounty
        {
            get { return _supervisionCounty; }
            set { _supervisionCounty = value.ScrubForDatabase(); }
        }

        public string SupervisionState
        {
            get { return _supervisionState; }
            set { _supervisionState = value.ScrubForDatabase(); }
        }

        public string SupervisionDate
        {
            get { return _supervisionDate; }
            set { _supervisionDate = value.ScrubForDatabase().FormatDateAsyyyyMMdd(); }
        }

        public string CommitmentLocation
        {
            get { return _commitmentLocation; }
            set { _commitmentLocation = value.ScrubForDatabase(); }
        }

        public string CommitmentCounty
        {
            get { return _commitmentCounty; }
            set { _commitmentCounty = value.ScrubForDatabase(); }
        }

        public string CommitmentState
        {
            get { return _commitmentState; }
            set { _commitmentState = value.ScrubForDatabase(); }
        }

        public string CommitmentDate
        {
            get { return _commitmentDate; }
            set { _commitmentDate = value.ScrubForDatabase().FormatDateAsyyyyMMdd(); }
        }

        public string ReleaseDate
        {
            get { return _releaseDate; }
            set { _releaseDate = value.ScrubForDatabase().FormatDateAsyyyyMMdd(); }
        }

        public string FBINumber
        {
            get { return _fBINumber; }
            set { _fBINumber = value.ScrubForDatabase(); }
        }

        public string BookingNumber
        {
            get { return _bookingNumber; }
            set { _bookingNumber = value.ScrubForDatabase(); }
        }

        public string DLNumber
        {
            get { return _dLNumber; }
            set { _dLNumber = value.ScrubForDatabase(); }
        }

        public string AmendedCharge
        {
            get { return _amendedCharge; }
            set { _amendedCharge = value.ScrubForDatabase(); }
        }

        public string AmendedCaseType
        {
            get { return _amendedCaseType; }
            set { _amendedCaseType = value.ScrubForDatabase(); }
        }

        public string AmendedDisposition
        {
            get { return _amendedDisposition; }
            set { _amendedDisposition = value.ScrubForDatabase(); }
        }

        public string AmendedDispositionDate
        {
            get { return _amendedDispositionDate; }
            set { _amendedDispositionDate = value.ScrubForDatabase().FormatDateAsyyyyMMdd(); }
        }

        public string Status
        {
            get { return _status; }
            set { _status = value.ScrubForDatabase(); }
        }

        public string Restitution
        {
            get { return _restitution; }
            set { _restitution = value.ScrubForDatabase(); }
        }

        public string SuspendedSentence
        {
            get { return _suspendedSentence; }
            set { _suspendedSentence = value.ScrubForDatabase(); }
        }

        public string Comment1
        {
            get { return _comment1; }
            set { _comment1 = value.ScrubForDatabase(); }
        }

        public string Comment2
        {
            get { return _comment2; }
            set { _comment2 = value.ScrubForDatabase(); }
        }

        public DateTime? DateAdded { get; set; }

        public DateTime? LastUpdateDate { get; set; }

        public int? SourceInfoID { get; set; }

        public int? BatchID { get; set; }

        public string Expungement_Flag
        {
            get { return _expungement_Flag; }
            set { _expungement_Flag = value.ScrubForDatabase(); }
        }

        public string CaseType_Classification
        {
            get { return _caseTypeClassification; }
            set { _caseTypeClassification = value.ScrubForDatabase(); }
        }

        public void AppendComment1(string text)
        {
            if (!string.IsNullOrEmpty(Comment1) && !string.IsNullOrEmpty(text))
            {
                Comment1 += "; " + text;
            }
        }

        public void AppendComment2(string text)
        {
            if (!string.IsNullOrEmpty(Comment2) && !string.IsNullOrEmpty(text))
            {
                Comment2 += "; " + text;
            }
        }

        public bool IsDuplicateOf(Offense that)
        {
            foreach (var propertyInfo in typeof(Offense).GetProperties())
            {
                var thisPropertyValue = GetPropertyValue(this, propertyInfo.Name);
                var thatPropertyValue = GetPropertyValue(that, propertyInfo.Name);

                var thatString = thatPropertyValue as String;
                if (thatString != null)
                {
                    var thisString = (String) thisPropertyValue;
                    if (thisString != thatString)
                    {
                        return false;
                    }
                }

                var thatNullableInt = thatPropertyValue as int?;
                if (thatNullableInt != null)
                {
                    var thisNullableInt = thisPropertyValue as int?;

                    if (thisNullableInt != thatNullableInt)
                    {
                        return false;
                    }
                }

                var thatNullableDateTime = thatPropertyValue as DateTime?;
                if (thatNullableDateTime != null)
                {
                    var thisNullableDateTime = thisPropertyValue as DateTime?;

                    if (thisNullableDateTime != thatNullableDateTime)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private static Object GetPropertyValue(object obj, string propName)
        {
            var propertyInfo = obj.GetType().GetProperty(propName);
            return propertyInfo != null ? propertyInfo.GetValue(obj, null) : null;
        }
    }
}
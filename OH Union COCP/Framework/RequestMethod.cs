﻿namespace InnovativeEnterprises
{
    public sealed class RequestMethod
    {
        public static readonly RequestMethod GET = new RequestMethod("GET");
        public static readonly RequestMethod POST = new RequestMethod("POST");
        public readonly string Name;

        private RequestMethod(string name)
        {
            Name = name;
        }
    }
}
﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace InnovativeEnterprises
{
    internal static class CaptchaHelper
    {
        private const string CAPTCHA_ONE_SERVICE_URL = "http://172.16.1.74/captchasolver.asmx";
        private const string CAPTCHA_TWO_SERVICE_URL = "http://172.16.1.93/captchasolver.asmx";
        private const string SERVICE_PASSWORD = "cap5o!ve";

        public static string SolveCaptchaV1(string captchaUri, CookieContainer cookies, WebProxy proxy, string sourceName)
        {
            Console.WriteLine("Solving Captcha v1..." + Environment.NewLine);

            try
            {
                // Get the captcha image.
                var request = (HttpWebRequest) WebRequest.Create(captchaUri);
                request.Method = "GET";
                request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";
                request.CookieContainer = cookies;
                if (proxy != null)
                {
                    request.Proxy = proxy;
                }
                var response = request.GetResponse();
                var dataStream = response.GetResponseStream();
                var captchaImageString = "";
                var buffer = new byte[100001];
                var numBytesToRead = buffer.Length;
                var numBytesRead = 0;
                var n = 0;
                while (numBytesToRead > 0)
                {
                    n = dataStream.Read(buffer, numBytesRead, 1000);
                    if (n == 0)
                    {
                        break;
                    }
                    numBytesRead += n;
                    numBytesToRead -= n;
                }
                var captchaImageBytes = new byte[numBytesRead + 1];
                for (var i = 0; i <= numBytesRead - 1; i++)
                {
                    captchaImageBytes[i] = buffer[i];
                }
                captchaImageString = Convert.ToBase64String(captchaImageBytes);
                dataStream.Close();
                response.Close();

                // Solve the captcha.
                var req = (HttpWebRequest) WebRequest.Create(CAPTCHA_ONE_SERVICE_URL);
                req.Method = "POST";
                req.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";
                req.CookieContainer = cookies;
                var postData = "";
                postData += "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
                postData += "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">";
                postData += "  <soap:Body>";
                postData += "    <GetCaptchaSolution xmlns=\"https://xml.innovativedatasolutions.com/DecaptchaWebService\">";
                postData += "      <imageData>" + captchaImageString + "</imageData>";
                postData += "      <timeout>60000</timeout>";
                postData += "      <domainName>" + sourceName + "</domainName>";
                postData += "      <servicePassword>" + SERVICE_PASSWORD + "</servicePassword>";
                postData += "    </GetCaptchaSolution>";
                postData += "  </soap:Body>";
                postData += "</soap:Envelope>";
                var byteArray = Encoding.UTF8.GetBytes(postData);
                req.ContentLength = byteArray.Length;
                req.ContentType = "text/xml; encoding='utf-8'";
                dataStream = req.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                try
                {
                    response = req.GetResponse();
                }
                catch (Exception ex)
                {
                    response = null;
                }
                dataStream = response.GetResponseStream();
                var reader = new StreamReader(dataStream);
                var responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
                var captchaText = "";
                if (responseFromServer.IndexOf("<captchatext>") > -1)
                {
                    try
                    {
                        var s = responseFromServer.IndexOf("<captchatext>");
                        var e = responseFromServer.IndexOf("</captchatext>", s + "<captchatext>".Length);
                        if (s == -1 || e == -1)
                        {
                            var ex = new ArgumentOutOfRangeException();
                            throw ex;
                        }

                        s += "<captchatext>".Length;

                        captchaText = responseFromServer.Substring(s, e - s);
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        captchaText = "";
                    }
                }

                return captchaText;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public static string SolveCaptchaV2(string SourceId, string SiteKey, string PageURL)
        {
            Console.WriteLine("Solving Captcha v2..." + Environment.NewLine);

            var wr = (HttpWebRequest) WebRequest.Create(CAPTCHA_TWO_SERVICE_URL);
            wr.Method = "POST";
            wr.ContentType = "application/soap+xml; charset=utf-8";
            wr.Timeout = 2 * 60 * 1000;

            var body = "";
            body += "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
            body += "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">";
            body += "<soap12:Body>";
            body += "<GetCaptchaSolutionV2 xmlns=\"https://xml.innovativedatasolutions.com/DecaptchaWebService\">";
            body += "<SourceId>" + SourceId + "</SourceId>";
            body += "<SiteKey>" + SiteKey + "</SiteKey>";
            body += "<PageURL>" + PageURL + "</PageURL>";
            body += "<Proxy></Proxy>";
            body += "<ProxyType></ProxyType>";
            body += "<servicePassword>" + SERVICE_PASSWORD + "</servicePassword>";
            body += "</GetCaptchaSolutionV2>";
            body += "</soap12:Body>";
            body += "</soap12:Envelope>";

            var encoding = new ASCIIEncoding();
            var data = encoding.GetBytes(body);

            wr.ContentLength = data.Length;

            using (var dataStream = wr.GetRequestStream())
            {
                dataStream.Write(data, 0, data.Length);
            }

            var responseXML = string.Empty;

            var response = wr.GetResponse();
            var responseStream = response.GetResponseStream();
            using (var responseReader = new StreamReader(responseStream))
            {
                responseXML = responseReader.ReadToEnd();
            }

            var s = responseXML.IndexOf("<gRecaptchaResponse>");
            var e = responseXML.IndexOf("</gRecaptchaResponse>", s + "<gRecaptchaResponse>".Length);
            s += "<gRecaptchaResponse>".Length;

            return responseXML.Substring(s, e - s);
        }
    }
}
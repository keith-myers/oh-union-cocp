﻿using System;

namespace InnovativeEnterprises
{
    internal class BadCaptchaException : Exception
    {
        public override string Message
        {
            get { return "Captcha attempts exceeded the maximum allowed."; }
        }
    }
}
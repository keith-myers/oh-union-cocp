﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;

namespace InnovativeEnterprises
{
    public abstract class DownloadHelper
    {
        private const string GENERAL_ERROR_TEXT = "~~~~~~~~ Error Occured ~~~~~~~~~~";
        private const string TOO_MANY_ERRORS_TEXT = "~~~~~~~~ Failed with too many errors ~~~~~~~~~~";
        private const int MAX_ERRORS = 50;
        private static string DownloadPath;
        private int _errorCount;
        public static bool IsRunningLocally
        {
            get { return DownloadPath.StartsWith("C:"); }
        }
        private readonly string _proxyUrl;
        private readonly string _proxyUsername;
        private readonly string _proxyPassword;
        protected WebProxy Proxy { get; set; }
        protected CookieContainer CookieContainer { get; private set; }
        protected string Html { get; set; }
        protected string BackToResultsLink { get; set; }
        protected Request Request { get; set; }
        protected Image Image { get; set; }
        protected Dictionary<string, string> DetailData { get; set; }

        //-----( Begin Court View )----------------------------------
        protected string JSessionId { get; set; }
        protected string EmbeddedLink { get; set; }
        //-----( End Court View )------------------------------------

        //-----( Begin ASP.NET )-------------------------------------
        protected string ViewState { get; set; }
        protected string EventValidation { get; set; }
        //-----( End ASP.NET)----------------------------------------

        //-----( Begin Paging )--------------------------------------
        protected int NumPages { get; set; }
        protected int NextPage { get; set; }
        protected int NumRecordsProcessed { get; set; }
        //-----( End Paging )----------------------------------------

        //-----( Begin RunTime Shutoff )-----------------------------
        protected DateTime RunTimeStart { get; set; }
        protected int RunTimeHours { get; set; }
        //-----( End Runtime Shutoff )-------------------------------

        //-----( Begin Captcha )-------------------------------------
        protected string CaptchaKey { get; set; }
        protected string CaptchaAnswer { get; set; }

        protected bool IsGoodCaptchaAnswer { get; set; }
        protected int FailedCaptchaAttempts { get; set; }
        protected const int MAX_FAILED_CAPTCHA_ATTEMPTS = 5;
        //-----( End Captcha )---------------------------------------

        //-----( Begin Letter Search )-------------------------------
        protected string LetterSearchFirstName { get; set; }
        protected string LetterSearchLastName { get; set; }
        //-----( End Letter Search )---------------------------------

        //-----( Begin Date Range Search )---------------------------
        protected DateTime DateSearchStart { get; set; }
        protected DateTime DateSearchEnd { get; set; }
        //-----( End Date Range Search )-----------------------------

        protected DownloadHelper(string downloadPath, string proxyUrl, string username, string password, DateTime runTimeStart, int runTimeHours)
        {
            DownloadPath = downloadPath;
            _proxyUrl = proxyUrl;
            _proxyUsername = username;
            _proxyPassword = password;
            Proxy = new WebProxy(proxyUrl) {Credentials = new NetworkCredential(username, password)};
            RunTimeStart = runTimeStart;
            RunTimeHours = runTimeHours;
            Directory.CreateDirectory(downloadPath + @"\Img");
            CookieContainer = new CookieContainer();
            NumPages = 1;
            NextPage = 2;
            DetailData = new Dictionary<string, string>();
        }

        protected void HandleError(Exception ex)
        {
            Proxy = new WebProxy(_proxyUrl) {Credentials = new NetworkCredential(_proxyUsername, _proxyPassword)};
            CookieContainer = new CookieContainer();

            if (++_errorCount > MAX_ERRORS)
            {
                throw new Exception(Environment.NewLine + TOO_MANY_ERRORS_TEXT);
            }
            
            WriteLine(GENERAL_ERROR_TEXT + Environment.NewLine + ex);

            if (ex.Message.Contains("503"))
            {
                WriteLine("Waiting 15 seconds after a 503...");
                Thread.Sleep(1000 * 15);
            }
        }

        protected void HandleBadCaptchaAnswer()
        {
            WriteLine(string.Format("Bad Captcha answer number {0}...", ++FailedCaptchaAttempts));

            if (FailedCaptchaAttempts > MAX_FAILED_CAPTCHA_ATTEMPTS)
            {
                FailedCaptchaAttempts = 0;
                throw new BadCaptchaException();
            }
        }

        protected static string GetViewstateValue(string html)
        {
            const string viewStateNameDelimiter = "__VIEWSTATE";
            const string valueDelimiter = "value=\"";

            var viewStateNamePosition = html.IndexOf(viewStateNameDelimiter, StringComparison.Ordinal);

            if (viewStateNamePosition < 0)
            {
                return "";
            }

            var viewStateValuePosition = html.IndexOf(valueDelimiter, viewStateNamePosition, StringComparison.Ordinal);
            var viewStateStartPosition = viewStateValuePosition + valueDelimiter.Length;
            var viewStateEndPosition = html.IndexOf("\"", viewStateStartPosition, StringComparison.Ordinal);

            return html.Substring(viewStateStartPosition, viewStateEndPosition - viewStateStartPosition).Trim();
        }

        protected static string GetEventValidationValue(string html)
        {
            const string eventValidationNameDelimiter = "__EVENTVALIDATION";
            const string valueDelimiter = "value=\"";

            var eventValidationNamePosition = html.IndexOf(eventValidationNameDelimiter, StringComparison.Ordinal);
            if (eventValidationNamePosition < 0)
            {
                return "";
            }

            var eventValidationValuePosition = html.IndexOf(valueDelimiter, eventValidationNamePosition, StringComparison.Ordinal);
            var eventValidationStartPosition = eventValidationValuePosition + valueDelimiter.Length;
            var eventValidationEndPosition = html.IndexOf("\"", eventValidationStartPosition, StringComparison.Ordinal);

            return html.Substring(eventValidationStartPosition, eventValidationEndPosition - eventValidationStartPosition).Trim();
        }

        public static void WriteLine(string input)
        {
            Console.WriteLine("{0} ({1}){2}", input, DateTime.Now, Environment.NewLine);
        }

        public static void SaveHtml(string html, string fullName, string uniqueId, string suffix)
        {
            File.WriteAllText(string.Format(@"{0}\{1}_{2}_{3}.html", DownloadPath, fullName.RemoveNonAlphaNumericCharacters(), uniqueId.RemoveNonAlphaNumericCharacters(), suffix), html);
        }

        public static void SavePhoto(Image image, string fullName, string uniqueId, string suffix)
        {
            image.Save(string.Format(@"{0}\Img\{1}_{2}_{3}.jpg", DownloadPath, fullName.RemoveNonAlphaNumericCharacters(), uniqueId.RemoveNonAlphaNumericCharacters(), suffix), ImageFormat.Jpeg);
            image.Dispose();
        }

        protected static bool IsTimeForAmShutoff()
        {
            return !IsRunningLocally && !IsRunningLocally && DateTime.Now.Hour == 9;
        }

        protected bool HasReachedRunTimeLimit()
        {
            var runTime = DateTime.Now - RunTimeStart;
            return runTime.Hours >= RunTimeHours;
        }

        protected void SetupPager(string startChunk, string endChunk, int numRecordsPerPage)
        {
            var pagerCountSnip = Html.Snip(startChunk, endChunk, false);
            if (pagerCountSnip == null)
            {
                return;
            }

            var chunks = pagerCountSnip.Split(new[] {"of"}, StringSplitOptions.None);

            if (chunks.Length < 2)
            {
                return;
            }

            var totalRecords = chunks[1].Trim();
            NumPages = (int) Math.Ceiling(decimal.Parse(totalRecords) / numRecordsPerPage);
        }

        protected void SetupPager(string elementId)
        {
            var pageCountSpanTag = new CQuery(Html).GetById(elementId);
            if (pageCountSpanTag != null)
            {
                var pageCountSpanTagText = pageCountSpanTag.Text();
                int numPages;
                int.TryParse(pageCountSpanTagText, out numPages);
                NumPages = numPages;
            }
        }

        protected static string GetRandomDoublePaddedTo18Digits()
        {
            return new Random().NextDouble().ToString(CultureInfo.CurrentCulture).PadRight(18, '0');
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace InnovativeEnterprises
{
    public abstract class ParseHelper
    {
        private static string DownloadPath;
        private readonly string _connectionString;
        private SqlConnection _connection;

        protected ParseHelper(string downloadPath, string connectionString)
        {
            DownloadPath = downloadPath;
            _connectionString = connectionString;
        }

        public void Run(Organization organization)
        {
            Run(organization.Source, organization.SourceName, organization.SourceState, organization.Category, organization.CountyOrJurisdiction);
        }

        public void Run(string source, string sourceName, string sourceState, string category, string countyOrJurisdiction)
        {
            Connect();

            foreach (var dataFilePath in GetDataFilePaths())
            {
                try
                {
                    var html = File.ReadAllText(dataFilePath);
                    var body = new CQuery(html).GetByElementName("BODY").Selection;

                    if (body == null)
                    {
                        continue;
                    }

                    ParseDataFile(body, source, sourceName, sourceState, category, countyOrJurisdiction);
                }
                catch (Exception ex)
                {
                    Disconnect();
                    throw new Exception("File: " + dataFilePath + Environment.NewLine + ex);
                }
            }

            Disconnect();
        }

        protected abstract void ParseDataFile(string body, string source, string sourceName, string sourceState, string category, string countyOrJurisdiction);

        // TODO: Summary
        public void WriteData<T>(T source)
        {
            var tableName = "";
            if (source.GetType() == typeof(Person))
            {
                var person = (Person) (Object) source;
                tableName += person.Source;
                tableName += "_Person_Staging";
            }
            else
            {
                var offense = (Offense) (Object) source;
                tableName += offense.Source;
                tableName += "_Offense_Staging";
            }

            using (var command = _connection.CreateCommand())
            {
                var commandText = new StringBuilder("INSERT INTO " + tableName + " VALUES (");

                var nullableIntType = typeof(int?);

                var properties = typeof(T).GetProperties();
                foreach (var propertyInfo in properties)
                {
                    var type = propertyInfo.PropertyType;
                    var name = propertyInfo.Name;
                    var value = GetPropertyValue(source, name);

                    if (type == nullableIntType)
                    {
                        commandText.AppendFormat("{0},", value ?? "null");
                    }
                    else
                    {
                        if (value != null)
                        {
                            commandText.AppendFormat("'{0}',", value);
                        }
                        else
                        {
                            commandText.Append("null,");
                        }
                    }
                }
                commandText.Remove(commandText.Length - 1, 1);
                commandText.Append(")");

                _connection.Open();
                command.CommandText = commandText.ToString();
                command.ExecuteNonQuery();
                _connection.Close();
            }
        }

        // TODO: Summary
        private static Object GetPropertyValue(object obj, string propName)
        {
            var propertyInfo = obj.GetType().GetProperty(propName);
            return propertyInfo != null ? propertyInfo.GetValue(obj, null) : null;
        }

        // TODO: Summary
        private static string[] GetDataFilePaths()
        {
            return Directory.GetFiles(DownloadPath);
        }

        // TODO: Summary
        private void Connect()
        {
            _connection = new SqlConnection(_connectionString);
        }

        // TODO: Summary
        private void Disconnect()
        {
            _connection.Dispose();
        }

        // TODO: Summary
        protected void SetPersonNameAndId(Person person, bool isLastFirst, string fullName, string extraIdentifier)
        {
            fullName = fullName.Replace("'", "''");
            var nameOrder = isLastFirst ? "lf" : "fl";

            _connection.Open();

            person.LastName = ExecuteScalar(string.Format("SELECT [dbo].[ParseName_v1] ('{0}', 'ln', '{1}')", fullName, nameOrder));
            person.FirstName = ExecuteScalar(string.Format("SELECT [dbo].[ParseName_v1] ('{0}', 'fn', '{1}')", fullName, nameOrder));
            person.MiddleName = ExecuteScalar(string.Format("SELECT [dbo].[ParseName_v1] ('{0}', 'mn', '{1}')", fullName, nameOrder));
            person.Generation = ExecuteScalar(string.Format("SELECT [dbo].[ParseName_v1] ('{0}', 'gn', '{1}')", fullName, nameOrder));

            _connection.Close();

            var nameForId = "";

            if (person.FirstName.Length < 5)
            {
                nameForId += person.FirstName;
            }
            else
            {
                nameForId += person.FirstName.Substring(0, 5);
            }

            if (person.LastName.Length < 5)
            {
                nameForId += person.LastName;
            }
            else
            {
                nameForId += person.LastName.Substring(0, 5);
            }

            if (person.MiddleName.Length >= 1)
            {
                nameForId += person.FirstName.Substring(0, 1);
            }

            person.ID = extraIdentifier + nameForId;
        }

        // TODO: Summary
        private string ExecuteScalar(string query)
        {
            using (var command = _connection.CreateCommand())
            {
                command.CommandText = query;
                return (string) command.ExecuteScalar();
            }
        }

        // TODO: Summary
        protected static void ConsolidateOffenses(List<Offense> otherOffensesForThisPerson, Offense thisOffense)
        {
            var isDuplicateOffense = false;

            foreach (var otherOffenseForThisPerson in otherOffensesForThisPerson)
            {
                if (thisOffense.IsDuplicateOf(otherOffenseForThisPerson))
                {
                    otherOffenseForThisPerson.Counts++;
                    isDuplicateOffense = true;

                    break;
                }
            }

            if (!isDuplicateOffense)
            {
                otherOffensesForThisPerson.Add(thisOffense);
            }
        }
    }
}
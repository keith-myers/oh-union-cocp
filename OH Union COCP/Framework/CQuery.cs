﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace InnovativeEnterprises
{
    public class CQuery
    {
        public List<string> Selections
        {
            get { return _selections; }
        }

        public string Selection
        {
            get { return _selections.SingleOrDefault(); }
        }

        private readonly List<string> _tempSelections = new List<string>();
        private readonly List<string> _selections = new List<string>();

        public CQuery(string html)
        {
            _selections.Add(html);
        }

        public CQuery GetByElementName(string elementName)
        {
            _tempSelections.Clear();

            foreach (var selection in _selections)
            {
                var startIndex = 0;
                var htmlLength = selection.Length;

                var elementOpeningTag = string.Format("<{0}", elementName);
                var elementClosingTag = string.Format("</{0}>", elementName);

                var numOpeningTags = 1;
                var numClosingTags = 0;

                while (true)
                {
                    startIndex = selection.IndexOf(elementOpeningTag, startIndex, StringComparison.OrdinalIgnoreCase);

                    if (startIndex == -1 || startIndex >= htmlLength)
                    {
                        break;
                    }

                    var endOfTagIndex = selection.IndexOf('>', startIndex);

                    while (numOpeningTags != numClosingTags)
                    {
                        var indexOfOpeningTag = selection.IndexOf(elementOpeningTag, endOfTagIndex + 1, StringComparison.OrdinalIgnoreCase);
                        var openingTagExists = indexOfOpeningTag != -1;
                        var indexOfClosingTag = selection.IndexOf(elementClosingTag, endOfTagIndex + 1, StringComparison.OrdinalIgnoreCase);
                        var closingTagExists = indexOfClosingTag != -1;

                        int indexOfTag;

                        if (closingTagExists && !openingTagExists || openingTagExists && closingTagExists && indexOfClosingTag < indexOfOpeningTag)
                        {
                            numClosingTags++;
                            indexOfTag = indexOfClosingTag;
                        }
                        else
                        {
                            numOpeningTags++;
                            indexOfTag = indexOfOpeningTag;
                        }
                        
                        if (indexOfTag == -1)
                        {
                            break;
                        }
                        endOfTagIndex = selection.IndexOf('>', indexOfTag);
                    }

                    _tempSelections.Add(selection.Substring(startIndex, endOfTagIndex - startIndex + 1));
                    startIndex = selection.IndexOf('>', startIndex);
                    numOpeningTags = 1;
                    numClosingTags = 0;
                }
            }

            _selections.Clear();
            _selections.AddRange(_tempSelections);

            return this;
        }

        public CQuery GetById(string id)
        {
            // 'FindById' is intended to operate on only one chunk of html.
            var selection = _selections.Single();

            // Find the index of the id itself. 
            var idIndex = selection.IndexOf(id, StringComparison.OrdinalIgnoreCase);
            if (idIndex == -1)
            {
                return null;
            }

            // Find the beginning of the opening tag.
            var startIndex = selection.LastIndexOf('<', idIndex);

            // Get the element name.
            var firstSpaceIndex = selection.IndexOf(' ', startIndex);
            var elementName = selection.Substring(startIndex + 1, firstSpaceIndex - startIndex).Trim().ToUpper();

            // Find the ending of the opening tag.
            var endOfTagIndex = selection.IndexOf('>', startIndex);

            // Find the closing tag. This digs through nested elements of the same name to find the correct ending tag.
            var numOpeningTags = 1;
            var numClosingTags = 0;
            while (numOpeningTags != numClosingTags)
            {
                var indexOfOpeningTag = selection.IndexOf('<' + elementName, endOfTagIndex, StringComparison.OrdinalIgnoreCase);
                var openingTagExists = indexOfOpeningTag != -1;
                var indexOfClosingTag = selection.IndexOf("</" + elementName, endOfTagIndex, StringComparison.OrdinalIgnoreCase);
                var closingTagExists = indexOfClosingTag != -1;

                int indexOfTag;

                if (closingTagExists && !openingTagExists || openingTagExists && closingTagExists && indexOfClosingTag < indexOfOpeningTag)
                {
                    numClosingTags++;
                    indexOfTag = indexOfClosingTag;
                }
                else
                {
                    numOpeningTags++;
                    indexOfTag = indexOfOpeningTag;
                }

                // Advance search to the end of the found tag and search for the next tag.
                if (indexOfTag == -1)
                {
                    break;
                }
                endOfTagIndex = selection.IndexOf('>', indexOfTag);

                //var debugSnip = selection.Substring(selection.LastIndexOf('<', indexOfTag));
            }

            var finishIndex = endOfTagIndex + 1;

            // Select the element.
            _tempSelections.Clear();
            _tempSelections.Add(selection.Substring(startIndex, finishIndex - startIndex));

            _selections.Clear();
            _selections.AddRange(_tempSelections);

            return this;
        }

        public string Text()
        {
            // This method operates on ONE selection.
            return _selections.Count != 1 ? "" : _selections.Single().Text();
        }

        /// <summary>
        ///     Operates on a selection of TRs and extracts the text of the TDs.<br />
        ///     Usage: var allTableCellText = new
        ///     CQuery(html).FindByElementName("TABLE").FindByElementName("TR").AllTableCellText();
        /// </summary>
        /// <returns>A grid of cell text in a list of lists format.</returns>
        public List<IList<string>> GetAllTableCellText()
        {
            var allTableCellText = new List<IList<string>>();

            var tableRows = GetByElementName("TR").Selections;
            foreach (var tableRow in tableRows)
            {
                var tableCellText = new List<string>();
                var cells = new CQuery(tableRow).GetByElementName("TD")._selections;

                foreach (var cell in cells)
                {
                    tableCellText.Add(cell.Text());
                }

                allTableCellText.Add(tableCellText);
            }

            return allTableCellText;
        }

        public static string CreateDetailHeaderTable(string header, string row)
        {
            return string.Format("<TABLE>{0}{1}</TABLE>", header, row);
        }
    }

    public static class CQueryExtensions
    {
        public static string Text(this string html)
        {
            var startIndex = html.IndexOf('>');
            if (startIndex == -1)
            {
                return "";
            }
            startIndex += 1;

            var finishIndex = html.LastIndexOf('<', html.Length - 1);

            return finishIndex < 0 ? "" : html.Substring(startIndex, finishIndex - startIndex).Trim();
        }

        public static string StoreData(this string html, string id, string data)
        {
            /*
             * Store the data as the first thing in the body if there IS a BODY.
             * If not, store it as the first thing in the HTML.
             */

            var startIndex = html.IndexOf("BODY", StringComparison.OrdinalIgnoreCase);
            if (startIndex != -1)
            {
                startIndex = html.IndexOf('>', startIndex) + 1;
            }
            else
            {
                startIndex = 0;
            }

            return html.Insert(startIndex, string.Format(@"<SECTION id=""{0}"" style=""color:fuchsia;border:solid 3px fuchsia"">{1}</SECTION>", id, data));
        }
    }
}
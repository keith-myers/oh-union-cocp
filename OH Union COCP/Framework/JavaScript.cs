﻿using System;

namespace InnovativeEnterprises
{
    public static class JavaScript
    {
        public static long GetCurrentTime()
        {
            var newYearsDay1970 = new DateTime(1970, 1, 1);
            var timeSinceThen = DateTime.Now.ToUniversalTime() - newYearsDay1970;

            return (int) (timeSinceThen.TotalMilliseconds + 0.5);
        }
    }
}
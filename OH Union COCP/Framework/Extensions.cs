﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;

namespace InnovativeEnterprises
{
    public static class Extensions
    {
        public static T RemoveFirstItem<T>(this List<T> list)
        {
            var firstItem = list[0];
            list.RemoveAt(0);
            return firstItem;
        }

        public static void CopyTo(this Stream input, Stream output)
        {
            var buffer = new byte[16 * 1024]; // Fairly arbitrary size...
            int bytesRead;

            while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, bytesRead);
            }
        }

        public static string RemoveNonAlphaNumericCharacters(this string s)
        {
            return s == null ? s : Regex.Replace(s, "[^a-zA-Z0-9]", "");
        }

        public static string RemoveNonNumericCharacters(this string s)
        {
            return s == null ? s : Regex.Replace(s, "[^0-9]", "");
        }

        public static string RemoveNonDateCharacters(this string s)
        {
            return s == null ? s : Regex.Replace(s, "[^0-9/]", "");
        }

        public static string RemoveNonCurrencyCharacters(this string s)
        {
            return s == null ? s : Regex.Replace(s, "[^0-9$,.]", "");
        }

        public static string FormatDateAsyyyyMMdd(this string s)
        {
            if (s == null)
            {
                return s;
            }

            DateTime dateTime;
            return DateTime.TryParse(s, out dateTime) ? dateTime.ToString("yyyMMdd") : "";
        }

        public static bool HasCurrencyValue(this string s)
        {
            if (s == null)
            {
                return false;
            }

            var numericCharacters = s.RemoveNonNumericCharacters();
            decimal numericValue;
            return decimal.TryParse(numericCharacters, out numericValue) && numericValue > 0;
        }

        public static string AppendCurrencyComment(this string s, string labelText, string text)
        {
            if (s == null)
            {
                return s;
            }

            if (!s.HasCurrencyValue())
            {
                return s;
            }

            s += string.Format("{0}{1}", labelText, text.RemoveNonCurrencyCharacters());

            return s;
        }

        public static string AppendComment(this string s, string labelText, string text)
        {
            if (s == null)
            {
                return s;
            }

            if (s != "")
            {
                s += "; ";
            }

            s += string.Format("{0}{1}", labelText, text);

            return s;
        }

        public static string ScrubForDatabase(this string s)
        {
            return s == null ? s : s.Replace("&nbsp;", "").Replace("'", "''");
        }

        public static string UrlEncode(this string s)
        {
            return s == null ? s : HttpUtility.UrlEncode(s);
        }

        public static string UrlDecode(this string s)
        {
            return s == null ? s : HttpUtility.UrlDecode(s);
        }

        public static string GetDay(this DateTime d)
        {
            return d.Day.ToString().PadLeft(2, '0');
        }

        public static string GetMonth(this DateTime d)
        {
            return d.Month.ToString().PadLeft(2, '0');
        }

        public static string GetYear(this DateTime d)
        {
            return d.Year.ToString();
        }

        public static bool SnipExists(this string s, string snip)
        {
            
            if (s == null)
            {
                return false;
            }

            return s.IndexOf(snip, StringComparison.InvariantCultureIgnoreCase) != -1;
        }

        public static string SnipWithoutEndChunk(this string s, string startChunk)
        {
            if (s == null)
            {
                return s;
            }
            
            var startIndex = s.IndexOf(startChunk, StringComparison.OrdinalIgnoreCase);
            if (startIndex == -1)
            {
                return "";
            }
            startIndex += startChunk.Length;

            return s.Substring(startIndex).Trim();
        }

        public static string Snip(this string s, string endChunk)
        {
            if (s == null)
            {
                return s;
            }

            const int startIndex = 0;
            var endIndex = s.IndexOf(endChunk, startIndex, StringComparison.OrdinalIgnoreCase);
            if (endIndex == -1)
            {
                return "";
            }

            return s.Substring(startIndex, endIndex - startIndex).Trim();
        }

        public static string Snip(this string s, string startChunk, string endChunk, bool keepChunks)
        {
            if (s == null)
            {
                return s;
            }

            var startIndex = s.IndexOf(startChunk, StringComparison.OrdinalIgnoreCase);
            if (startIndex == -1)
            {
                return "";
            }
            if (!keepChunks)
            {
                startIndex += startChunk.Length;
            }

            var endIndex = s.IndexOf(endChunk, startIndex, StringComparison.OrdinalIgnoreCase);
            if (endIndex == -1)
            {
                return "";
            }
            if (keepChunks)
            {
                endIndex += endChunk.Length;
            }

            return s.Substring(startIndex, endIndex - startIndex).Trim();
        }

        public static string SnipInReverse(this string s, string startChunk, string endChunk)
        {
            if (s == null)
            {
                return s;
            }

            var endIndex = s.IndexOf(startChunk, StringComparison.OrdinalIgnoreCase);
            if (endIndex == -1)
            {
                return "";
            }

            var startIndex = s.LastIndexOf(endChunk, endIndex, StringComparison.OrdinalIgnoreCase);
            if (startIndex == -1)
            {
                return "";
            }
            startIndex += endChunk.Length;

            return s.Substring(startIndex, endIndex - startIndex).Trim();
        }

        public static List<string> SnipAll(this string s, string startChunk, string endChunk)
        {
            var snips = new List<string>();

            if (s == null)
            {
                return snips;
            }

            var pointer = 0;

            while (pointer < s.Length)
            {
                var startIndex = s.IndexOf(startChunk, pointer, StringComparison.OrdinalIgnoreCase);
                if (startIndex == -1)
                {
                    break;
                }
                startIndex += startChunk.Length;

                var endIndex = s.IndexOf(endChunk, startIndex, StringComparison.OrdinalIgnoreCase);
                if (endIndex == -1)
                {
                    break;
                }

                snips.Add(s.Substring(startIndex, endIndex - startIndex).Trim());
                pointer = endIndex + endChunk.Length;
            }

            return snips;
        }

        public static string TrimToLength(this string s, int length)
        {
            return s != null && s.Length > length ? s.Substring(0, length) : s;
        }
    }
}
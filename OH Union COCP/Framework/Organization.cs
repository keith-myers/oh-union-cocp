﻿namespace InnovativeEnterprises
{
    public class Organization
    {
        public string Name { get; private set; }
        public int Index { get; private set; }
        public string Source { get; private set; }
        public string SourceName { get; private set; }
        public string SourceState { get; private set; }
        public string Category { get; private set; }
        public string CountyOrJurisdiction { get; private set; }

        public Organization(int index, string name, string source, string sourceName, string sourceState, string category, string countryOrJurisdiction)
        {
            Index = index;
            Name = name;
            Source = source;
            SourceName = sourceName;
            SourceState = sourceState;
            Category = category;
            CountyOrJurisdiction = countryOrJurisdiction;
        }
    }
}